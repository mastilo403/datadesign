<?php

require 'connect.php';

$id = isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : 0;

$delete = $pdo->prepare("delete from users where id = ?");
$delete->execute([$id]);

if($delete) {
    echo json_encode([
        "success" => 1,
        "id" => $id,
    ]);
} else {
    echo json_encode([
        "success" => 0,
        "errorText" => "Deletion Error!"
    ]);
}




