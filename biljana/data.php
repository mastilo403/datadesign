<?php
    require 'connect.php';

$id = isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : 0;
$name = isset($_POST['name']) && !empty($_POST['name']) ? $_POST['name'] : "";
$lastname = isset($_POST['lastname']) && !empty($_POST['lastname']) ? $_POST['lastname'] : "";
$address = isset($_POST['address']) && !empty($_POST['address']) ? $_POST['address'] : "";
$mail = isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : "";
$phone = isset($_POST['phone']) && !empty($_POST['phone']) ? $_POST['phone'] : "";
$action = isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : "";

if($action == "save") {
    $id = $pdo->query("select max(id)+1 id from users")->fetch()['id'];
    $insert = $pdo->prepare("insert into users (id, first_name, last_name, address, mail, phone) values (?, ?, ?, ?, ?, ?)");
    
    $insert->execute([$id, $name, $lastname, $address, $mail, $phone]);
    
    if($insert) {
        echo json_encode([
            "success" => 1,
            "id" => $id,
            "name" => $name,
            "lastname" => $lastname,
            "address" => $address,
            "mail" => $mail,
            "phone" => $phone,
            "action" => $action,
            ]
        );
    } else {
        echo json_encode([
            "success" => 0,
            "errorText" => "Insertion error!"
            ]
        );
    }
    
} else if($action == "update") {
    
    $update = $pdo->prepare("update users set first_name = ?, last_name = ?, address = ?, mail = ?, phone = ? where id = ?");
    $update->execute([$name, $lastname, $address, $mail, $phone, $id]);
    
    if($update) {
        echo json_encode([
            "success" => 1,
            "id" => $id,
            "name" => $name,
            "lastname" => $lastname,
            "address" => $address,
            "mail" => $mail,
            "phone" => $phone,
            "action" => $action,
            ]
        );
    } else {
        echo json_encode([
            "success" => 0,
            "errorText" => "Update error!"
            ]
        );
    }
    
    
} else if($action == 'delete_selected_records') {
    $selected_ids = isset($_POST['selected_ids']) && !empty($_POST['selected_ids']) ? $_POST['selected_ids'] : 0;

    $delete_selected = $pdo->prepare("delete from users where id in ($selected_ids)");
    $delete_selected->execute([$selected_ids]);
    
    if($delete_selected) {
        echo json_encode([
            "success" => 1,
            "id" => $selected_ids,
        ]);
    } else {
        echo json_encode([
            "success" => 0,
            "errorText" => "Deletion Error!"
        ]);
    }
}
