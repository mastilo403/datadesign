<?php 
    require 'connect.php';
    $page = isset($_GET['page']) && !empty($_GET['page'] ) ? $_GET['page'] : 1;
    $per_page = 5;
    $limitFrom = ($page - 1) * 5;
    $limitTo = $limitFrom + 5;
    
    $where_condition = "";
    
    // first name filter //
    if(isset($_POST['first_name_filter'])) {
        $first_name_filter = $_POST['first_name_filter'];
    }else {
        $first_name_filter = "";
    }
    
    if(!empty($first_name_filter)) {
        if(empty($where_condition)) {
            $where_condition = " where upper(first_name) like upper('%$first_name_filter%') ";
        }else{
            $where_condition .= " and upper(first_name) like upper('%$first_name_filter%') ";
        }  
    }
    
    // last name filter //
    if(isset($_POST['last_name_filter'])) {
        $last_name_filter = $_POST['last_name_filter'];
    }else {
        $last_name_filter = "";
    }
    
    if(!empty($last_name_filter)) {
        if(empty($where_condition)){
            $where_condition = " where upper(last_name) like upper('%$last_name_filter%') ";
        }else {
            $where_condition .= " and upper(last_name) like upper('%$last_name_filter%') ";
        }  
    }
    
    // address filter //
    if(isset($_POST['address_filter'])) {
        $address_filter = $_POST['address_filter'];
    }else {
        $address_filter = "";
    }
    
    if(!empty($address_filter)) {
        if(empty($where_condition)){
            $where_condition = " where upper(address) like upper('%$address_filter%') ";
        }else {
            $where_condition .= " and upper(address) like upper('%$address_filter%') ";
        }  
    }
    
    // mail filter //
     if(isset($_POST['mail_filter'])) {
        $mail_filter = $_POST['mail_filter'];
    }else {
        $mail_filter = "";
    }
    
    if(!empty($mail_filter)) {
        if(empty($where_condition)){
            $where_condition = " where upper(mail) like upper('%$mail_filter%') ";
        }else {
            $where_condition .= " and upper(mail) like upper('%$mail_filter%') ";
        }  
    }
    
    // phone filter //
     if(isset($_POST['phone_filter'])) {
        $phone_filter = $_POST['phone_filter'];
    }else {
        $phone_filter = "";
    }
    
    if(!empty($phone_filter)) {
        if(empty($where_condition)){
            $where_condition = " where upper(phone) like upper('%$phone_filter%') ";
        }else {
            $where_condition .= " and upper(phone) like upper('%$phone_filter%') ";
        }  
    }
    
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Datadesign</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <style>
            ul.pagination li.active {
                display: inline-block;
                min-width: 30px;
                padding: 5px;
                background-color: #0b51c5;
                color: white;
                text-align: center;
                border: thin solid #fff;
            }

            ul.pagination li a{
                display: inline-block;
                min-width: 30px;
                padding: 5px;
                background-color: #fefefe;
                color: #0b51c5;
                text-align: center;
                border: thin solid #0b51c5;
            }

        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Bootstrap Snipp for Datatable</h4>
                            <button type="button" class="btn btn-primary pull-right" id="add_new" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-ok-sign"></span>New record</button>
                            <button type="button" class="btn btn-danger pull-right" id="delete_selected"><span class="glyphicon glyphicon-ok-sign"></span>Delete selected</button>
                            
                            <div class="table-responsive" style="margin-top:10px;">
                                
                                    <table id="mytable" class="table table-bordred table-striped">
                                        <thead>
                                            <th><input type="checkbox" id="checkall" /></th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Address</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </thead>
                                        <tbody>
                                        <form id="main_table" name="main_form" method="post" action="">
                                            <tr>
                                                <td></td>
                                                <td><input type="text" id="first_name_filter" name="first_name_filter" class="form-control" placeholder="first name filter" value="<?php echo $first_name_filter; ?>"></td>
                                                <td><input type="text" id="last_name_filter" name="last_name_filter" class="form-control" placeholder="last name filter" value="<?php echo $last_name_filter; ?>"></td>
                                                <td><input type="text" id="address_filter" name="address_filter" class="form-control" placeholder="address filter" value="<?php echo $address_filter; ?>"></td>
                                                <td><input type="text" id="mail_filter" name="mail_filter" class="form-control" placeholder="mail filter" value="<?php echo $mail_filter; ?>"></td>
                                                <td><input type="text" id="phone_filter" name="phone_filter" class="form-control" placeholder="phone filter" value="<?php echo $phone_filter; ?>"></td>
                                                <td></td>
                                                <td><input type="submit" style="display:none" id="submit" name="submit" value="submit"></td>
                                            </tr>
                                        </form>

                                            <?php
                                                $row_number = $pdo->query("select count(id) row_number from users $where_condition")->fetch()['row_number'];
                                                $result = $pdo->query("select id, first_name, last_name, address, mail, phone from users $where_condition order by id desc limit $limitFrom,$limitTo")->fetchAll();
                                                foreach ($result as $r) {
                                            ?>
                                                    <tr id="tr_<?php echo $r['id'] ?>">
                                                        <td id="del_checkbox_<?php echo $r['id'] ?>"><input type="checkbox" class="checkthis" data-id="<?php echo $r['id'] ?>"/></td>
                                                        <td id="id_<?php echo $r['id'] ?>" style="display: none;"><?php echo $r['id'] ?></td>
                                                        <td id="name_<?php echo $r['id'] ?>"><?php echo $r['first_name'] ?></td>
                                                        <td id="lastname_<?php echo $r['id'] ?>"><?php echo $r['last_name'] ?></td>
                                                        <td id="address_<?php echo $r['id'] ?>"><?php echo $r['address'] ?></td>
                                                        <td id="mail_<?php echo $r['id'] ?>"><?php echo $r['mail'] ?></td>
                                                        <td id="phone_<?php echo $r['id'] ?>"><?php echo $r['phone'] ?></td>
                                                        <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs updateRecord" id="update_<?php echo $r['id'] ?>" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs deleteRecord" id="delete_<?php echo $r['id'] ?>" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                                    </tr>
                                            <?php }?>    
                                        </tbody>
                                    </table>
                        
                        <div class="clearfix"></div>
                            <ul class="pagination pull-right">
                            <?php
                            $counter = intdiv($row_number, $per_page);

                            for($i = 0; $i <= $counter; $i ++){

                                $k = $i+1;
                                if($i+1 != $page){
                                    echo '<li><a href="?page='.$k.'">'.$k.'</a></li>';
                                }else{
                                    echo '<li class="active">'.$k.'</li>';
                                }
                            }


                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="entry-from" class="entry-from">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group" style="display:none">
                                <input class="form-control " id="id" name="id" type="text" placeholder="ID">
                            </div>
                            <div class="form-group">
                                <input class="form-control " id="name" name="name" type="text" placeholder="First name">
                            </div>
                            <div class="form-group">
                                <input class="form-control " id="lastname" name="lastname" type="text" placeholder="Last name">
                            </div>
                            <div class="form-group">
                                <textarea rows="2" class="form-control" id="address" name="address" placeholder="Addresss"></textarea>
                            </div>
                            <div class="form-group">
                                <input class="form-control " id="mail" name="mail" type="text" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input class="form-control " id="phone" name="phone" type="text" placeholder="Contact">
                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-warning btn-lg" id="update" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span>Update</button>
                            <button type="button" class="btn btn-warning btn-lg" id="save" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span>Save</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content --> 
            </div>
          <!-- /.modal-dialog --> 
        </div>
        
        <script>
            
            function ajax_call(action,e) {
                e.preventDefault();
                   
                   var data = $("#entry-from").serialize();
                   data = data + "&action="+action;
                   
                   $.ajax({
                       method:"POST",
                       url: "data.php",
                       data: data,
                       dataType: "json",
                       beforeSend: function() {
                           console.log("Loading...");
                       },
                       success: function(result) {
                            if(result.success == 1) {
                               if(action == "save"){
                                        $('#mytable').prepend('<tr id="tr_'+result.id+'"><td id="del_checkbox_'+result.id+'"><input type="checkbox" class="checkthis" /></td><td id="id_'+result.id+'" style="display: none;">'+result.id+'</td><td id="name_'+result.id+'">'+result.name+'</td><td id="lastname_'+result.id+'">'+result.lastname+'</td><td id="address_'+result.id+'">'+result.address+'</td><td id="mail_'+result.id+'">'+result.mail+'</td><td id="phone_'+result.id+'">'+result.phone+'</td><td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs updateRecord" id="update_'+result.id+'" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td><td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs deleteRecord" id="delete_'+result.id+'" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></p></td></tr>');
                                        $(".close").click();

                               } else if(action == "update") {
                                    console.log(result);

                                    $(".close").click();

                                    $('#tr_' + result.id).fadeOut(500);

                                    $('#name_' + result.id).text(result.name);
                                    $('#lastname_' + result.id).text(result.lastname);
                                    $('#address_' + result.id).text(result.address);
                                    $("#mail_" + result.id).text(result.mail);
                                    $("#phone_" + result.id).text(result.phone);

                                    $("#tr_"+result.id).fadeIn(500);
                               }
                           
                           } else {
                               $.alert(result.errorText);
                           }
                           

                       },
                       error : function(err) {
                           console.log("Error!!!");
                           console.log(err);
                       },
                       complete : function() {
                           console.log("Completed...");
                       }
                       
                   });
            }
            
            $(document).ready(function() { 
            
            
                $("#checkall").on('click', function () {
                    if(this.checked) {
                        $(".checkthis").each(function() {
                            this.checked = true;
                        });
                    }else {
                        $(".checkthis").each(function() {
                            this.checked = false;
                        });
                    }
                });
            
            
//                $(document).on('click', '#checkall', function (event) {
//                    $(".checkthis").prop('checked', $(this).prop("checked"));
//                    if($(this).prop("checked"))
//                        $(".checkthis").parent('td').parent('tr').addClass('tr_select');
//                    else
//                        $(".checkthis").parent('td').parent('tr').removeClass('tr_select');
//                });
            
            });
            
            
                $(document).on("click", ".updateRecord", function() {
                     $("#save").hide();
                    $("#update").show();
                    var rowID = $(this).attr("id").substring(7);
                    
                    var id = $("#id_"+rowID).text();
                    var name = $("#name_"+rowID).text();
                    var lastname = $("#lastname_"+rowID).text();
                    var address = $("#address_"+rowID).text();
                    var mail = $("#mail_"+rowID).text();
                    var phone = $("#phone_"+rowID).text();
                    
                    $("#id").val(id);
                    $("#name").val(name);
                    $("#lastname").val(lastname);
                    $("#address").val(address);
                    $("#mail").val(mail);
                    $("#phone").val(phone);
          
                });
                
                $(document).on("click", "#save", function(e) {
                    ajax_call("save", e);
                });
                $(document).on("click", "#update", function(e) {
                    ajax_call("update", e);
                });
                
                $(document).on("click", ".deleteRecord", function() {
                    var rowID = $(this).attr("id").substring(7);
                    
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Do you want to delete this record?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    method : "POST",
                                    url : "delete.php",
                                    data : {
                                        id: rowID
                                    },
                                    dataType : "json",
                                    beforeSend : function() {
                                        console.log("Deleting...");
                                    },
                                    success : function(result) {
                                        if(result.success == 1) {
                                            $.alert('Deleted!');
                                            $("#tr_"+rowID).fadeOut().remove();
                                        } else {
                                            $.alert(result.errorText);
                                        }
                                        
                                    },
                                    error : function(err) {
                                        console.log(err);
                                    },
                                    complete : function() {
                                        console.log("Completed...");
                                    }
                                });
                            },
                            cancel: function () {
                                $.alert('Canceled!');
                            }
                        }
                    });
                });
                
                $(document).on("click", "#delete_selected", function() {
                    var selected_ids = "";

                    console.log(selected_ids);
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Do you want to delete these records?',
                        buttons: {
                            confirm: function () {
                                
                                    $(".checkthis:checked").each(function() {
                                        var selected_id = $(this).data("id");
                                        if(selected_ids=="") {
                                            selected_ids += selected_id;
                                        }else {
                                            selected_ids += "," + selected_id;
                                        }
                                    });
                                    
                                    $.ajax({
                                    method : "POST",
                                    url : "data.php",
                                    data : {
                                        selected_ids: selected_ids,
                                        action:'delete_selected_records'
                                    },
                                    dataType : "json",
                                    beforeSend : function() {
                                        console.log("Deleting...");
                                    },
                                    success : function(result) {
                                        if(result.success == 1) {
                                            $.alert('Deleted!');
                                            location.reload();
                                        } else {
                                            $.alert(result.errorText);
                                        }
                                        
                                    },
                                    error : function(err) {
                                        console.log(err);
                                    },
                                    complete : function() {
                                        console.log("Completed...");
                                    }
                                });
                                
                            },
                            cancel: function () {
                                $.alert('Canceled!');
                            }
                        }
                    });
                });
                
                $(document).on("click", "#add_new", function(e) {
                    e.preventDefault();
                    $("#save").show();
                    $("#update").hide();
                    
                    $("#id").val("");
                    $("#name").val("");
                    $("#lastname").val("");
                    $("#address").val("");
                    $("#mail").val("");
                    $("#phone").val("");
                    
                });
            
        </script>
    </body>
</html>
		
        
    
    
    
    
    
    

    
   
    
   
    
        

                
            


        
    
        
    
    
    


