<?php
require 'connect.php';

$id = isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : 0;
$first_name = isset($_POST['first_name']) && !empty($_POST['first_name']) ? $_POST['first_name'] : "";
$last_name = isset($_POST['last_name']) && !empty($_POST['last_name']) ? $_POST['last_name'] : "";
$address = isset($_POST['address']) && !empty($_POST['address']) ? $_POST['address'] : "";
$email = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : "";
$contact = isset($_POST['contact']) && !empty($_POST['contact']) ? $_POST['contact'] : "";

$action = isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : "";

if($action == 'save'){

$id = $pdo->query("select max(id)+1 id from users")->fetch()['id'] ;
$insert = $pdo->prepare(" insert into users(id, first_name, last_name, address, mail, phone )values(?, ?, ?, ?, ?, ?)");
$insert->execute([$id, $first_name,  $last_name, $address, $email,$contact ]);

if($insert){

  echo json_encode([
      success => 1,
      id => $id,
      first_name => $first_name,
      last_name => $last_name,
      address => $address,
      email => $email,
      contact => $contact

  ]);

}else{

  echo json_encode([
      success => 0,
      tekstGreske => "Doslo je do greske"
  ]);

}

}else if($action == 'update'){

$update = $pdo->prepare(" update users set first_name = ?, last_name= ?, address= ?, mail= ?, phone= ? where id= ?");
$update->execute([$first_name, $last_name,$address,$email,$contact,$id]);

if($update){

  echo json_encode([
      success => 1,
      id => $id,
      first_name => $first_name,
      last_name => $last_name,
      address => $address,
      email => $email,
      contact => $contact

  ]);

}else{

  echo json_encode([
      success => 0,
      tekstGreske => "Doslo je do greske"
  ]);

}

}else if($action == 'delete2'){
$selektovani_id = isset($_POST['selektovani_id']) && !empty($_POST['selektovani_id']) ? $_POST['selektovani_id'] : null;

$delete = $pdo->prepare(" delete from users where id in(?)");
$delete->execute([$selektovani_id]);

if($delete){

  echo json_encode([
      success => 1,
      id => $selektovani_id
  ]);

}else{

  echo json_encode([
      success => 0,
      tekstGreske => "Doslo je do greske"
  ]);


}
}
